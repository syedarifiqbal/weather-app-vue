const Search = { 
    props: ["keyword"],
    template: `
        <div>

            <div class="row">

                <div class="col-md-12" v-if="isLoading">
                    <div class="loading-container">
                        <div class="loading"></div>
                        <div id="loading-text">loading</div>
                    </div>
                </div>

                <div class="col-md-4" v-if="!isLoading" v-for="city in data" :key="city.woeid">
                    <weather :city="city"></weather>
                </div>
    
            </div>

        </div>
    `,
    data(){
        return {
            title: 'Home page',
            data: [],
            isLoading: true
        }
    },
    mounted() {
        this.loadData();
    },
    watch: {
        keyword: function (id) {
            this.loadData();
        }
    },
    methods: {
        loadData: function(){
            var that = this;
            axios.get(`/weather.php?command=search&keyword=${this.keyword}`).then(function(data){
                if(data.data.length == 0)
                {
                    that.$router.push({ path: '/404' });
                    return;
                }
                that.data = data.data;
                that.isLoading = false;
            });
        }
    }
};