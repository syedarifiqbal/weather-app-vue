const Home = { 
    template: `
        <div>

            <div class="row">

                <div class="col-md-12" v-if="isLoading">
                    <div class="loading-container">
                        <div class="loading"></div>
                        <div id="loading-text">loading</div>
                    </div>
                </div>

                <div class="col-md-4" v-if="!isLoading" v-for="city in data" :key="city.woeid">
                    <weather :city="city"></weather>
                </div>
    
            </div>

        </div>
    `,
    data(){
        return {
            title: 'Home page',
            data: [],
            isLoading: true
        }
    },
    mounted() {
        
        var that = this;

        let cities = ['Istanbul', 'Berlin', 'London', 'Helsinki', 'Dublin', 'Vancouver'];
        // var cities = ['Istanbul', 'Berlin'];

        cities.forEach(function(city){

            axios.get(`/weather.php?command=search&keyword=${city}`).then(function(data){

                that.data.push(data.data[0]);

                if (that.data.length == cities.length) 
                {
                    that.isLoading = false;
                }

            });

        });


    }
};