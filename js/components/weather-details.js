const WeatherDetail = { 
    props: ['woeid'],
    template: `
        <div>

            <div class="row">

                <div class="col-md-12" v-if="isLoading">
                    <div class="loading-container">
                        <div class="loading"></div>
                        <div id="loading-text">loading</div>
                    </div>
                </div>

                <div class="col-md-12" v-if="!isLoading">

                    <div class="row">

                        <div class="col-md-4">

                            <img class="img-fluid" :src="image(today.weather_state_abbr)" :alt="city.title" />

                        </div>

                        <div class="col-md-8">
                            
                            <h1>{{ d.title }}</h1>
                            <h3 class="text-mute">Today temperature: {{ tofixed(today.the_temp) }}</h3>
                            <h3 class="text-mute">Min temperature: {{ tofixed(today.min_temp) }}</h3>
                            <h3 class="text-mute">Max temperature: {{ tofixed(today.max_temp) }}</h3>

                        </div>
    
                    </div>

                    <div class="row mt-5">

                        <div class="col-md-4" v-for="d in forecast" :key="d.id">

                            <weather :city="city" :d="d" :showDay="true"></weather>

                        </div>

                    </div>

                </div>
    
            </div>

        </div>
    `,
    data(){
        return {
            title: 'Weather Details Page',
            d: {},
            city: {},
            today: {},
            forecast: {},
            isLoading: true
        }
    },
    mounted() {
        
        var that = this;

        axios.get(`/weather.php?command=location&woeid=${this.woeid}`).then(function(fdata){
            that.d = fdata.data;
            that.city = fdata.data.parent;
            that.today = fdata.data.consolidated_weather[0];
            that.forecast = fdata.data.consolidated_weather;
            that.forecast.splice(0, 1);
            that.isLoading = false;
        });
    },

    methods: {
        image: function (name) {
          return 'https://www.metaweather.com/static/img/weather/' + name + '.svg';
        },
        tofixed: function (value) {
          return parseInt(value).toFixed(2);
        }
    }

};