const WeatherComponent = Vue.component('weather', {

    props: ["city", "d", "showDay"],

    template: `<div class="card mb-3" style="">
                    <div class="col-md-12" v-if="isLoading">
                        <div class="loading-container">
                            <div class="loading"></div>
                            <div id="loading-text">loading</div>
                        </div>
                    </div>
                    <div class="card-body" v-if="!isLoading">
                        <img class="card-img-top" :src="image" :alt="city.title">
                    </div>
                    <div class="card-body" v-if="!isLoading">
                        <h5 class="card-title" v-if="!showDay">{{ city.title }}</h5>
                        <h5 class="card-title" v-if="showDay">{{ day }}</h5>
                        <p class="card-text"></p>
                    </div>
                    <ul class="list-group list-group-flush" v-if="!isLoading">
                        <li class="list-group-item">Today's temperature: {{ the_temp }}°</li>
                        <li class="list-group-item">Min temperature: {{ min_temp }}°</li>
                        <li class="list-group-item">Max temperature: {{ max_temp }}°</li>
                    </ul>
                    <div class="card-body" v-if="!isLoading && !showDay">
                        <router-link :to="'/weather/'+city.woeid" class="btn btn-primary">View Details</router-link>
                    </div>
                </div>`,

    data: function () {
        return {
            forecast: {},
            isLoading: false,
        }
    },

    created: function(){
        var that = this;

        if (!that.d) {
            that.isLoading = true;
            axios.get(`/weather.php?command=location&woeid=${this.city.woeid}`).then(function(fdata){
                that.forecast = fdata.data;

                that.d = that.forecast.consolidated_weather[0];

                that.isLoading = false;
            });
        }

    },

    computed: {
        image: function () {
          return 'https://www.metaweather.com/static/img/weather/' + this.d.weather_state_abbr + '.svg';
        },
        min_temp: function () {
            if (!this.d) { return ''; }
            return parseInt(this.d.min_temp).toFixed(2);
        },
        max_temp: function () {
            if (!this.d) { return ''; }
            return parseInt(this.d.max_temp).toFixed(2);
        },
        the_temp: function () {
            if (!this.d) { return ''; }
            return parseInt(this.d.the_temp).toFixed(2);
        },
        day: function () {
            if (!this.d) { return ''; }
            var days = ["Sunday", "Monday", "Tuesday","Wednesday", "Thursday", "Friday", "Saturday"];
            return days[(new Date(this.d.applicable_date)).getDay()];
        }
    }

});