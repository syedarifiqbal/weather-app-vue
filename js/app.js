const NotFoundComponent = { template: '<div>Not found</div>' };

const routes = [
    { path: '/', component: Home },
    { path: '/weather/:woeid', component: WeatherDetail, props: true },
    { path: '/search/:keyword', component: Search, props: true },
    { path: '*', component: NotFoundComponent }
];

const router = new VueRouter({
    mode: 'history',
    routes: routes
});

const app = new Vue({
    router: router,
    
    created() {
        console.log('this is loadded the app');
    },
    
    data: {
        keyword: ''
    },

    methods: {

        search: function(){
            if (this.keyword.trim().length == 0) {
                this.$router.push('home');
                return;
            }

            this.$router.push({ path: '/search/' + this.keyword });

        }

    }
}).$mount('#app');
